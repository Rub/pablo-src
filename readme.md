# Pablo

Design engaging images for your social media posts in under 30 seconds

### Version
0.0.1

### Tech

Pablo uses a number of open source projects to work properly:

* [node.js] - evented I/O for the backend
* [Express] - fast node.js network app framework
* [Gulp] - the streaming build system
* [jQuery] - for DOM manipulations
* [Jade] - for generating HTML
* [Bower] - for javascript libs

### Installation

You need Gulp and Bower installed globally:

```sh
$ npm i -g gulp
$ npm i -g bower
```

```sh
$ git clone [git-repo-url] Pablo
$ cd Pablo
$ npm i
$ bower i
```
for build and run server run this command:

```sh
$ gulp
```

### Development

Want to contribute? Great!

Pablo uses Gulp + Webpack for fast developing.
Make a change in your file and instantanously see your updates!

Open your favorite Terminal and run this command.

```sh
$ gulp
```

### File structure

#### Main source

> src - all source files

> src/templates - jade files (app pages)

> app.js - main server file

> gulpfile.js - Gulp config file

> bower.json - Bower config file

> admins.json - this file contains admin login pass for panels

>  quote.json - quotes are stored in this file

> Procfile - Heroku config file

> readme.md - this doc file

#### After Build

** Note **: for build just run `gulp`

> ** public ** - all generated public files (css, js, images, fonts and etc)

> ** views ** - generated html files

License
----

MIT

**Free Software, Hell Yeah!**

[node.js]:http://nodejs.org
[jQuery]:http://jquery.com
[express]:http://expressjs.com
[Gulp]:http://gulpjs.com
[Jade]:http://jade-lang.com/
[Bower]:http://bower.io/
