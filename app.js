var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var app = module.exports.app = exports.app = express();
var qs = require('querystring');
var fs = require('fs');
var auth = require('basic-auth');
var multiparty = require('multiparty');
/*
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
*/
app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

var admins = JSON.parse(fs.readFileSync('admins.json', 'utf8'));

function isJson(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

function json_quote(string) {
  var cx = /[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
      escapable = /[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
      meta = { // table of character substitutions
          '\b': '\\b',
          '\t': '\\t',
          '\n': '\\n',
          '\f': '\\f',
          '\r': '\\r',
          '"' : '\\"',
          '\\': '\\\\'
      };
  escapable.lastIndex = 0;
  return escapable.test(string) ? '"' + string.replace(escapable, function (a) {
      var c = meta[a];
      return typeof c === 'string' ? c :
          '\\u' + ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
  }) + '"' : '"' + string + '"';
}

function removeThumbs(arr){
	var index = arr.indexOf('Thumbs.db');
	if(index > -1) arr.splice(index, 1);
	return arr;
}

app.use(function (req, res, next) {
  res.removeHeader("x-powered-by");
  next();
});

app.get('/changeQuotes', function(req, res, next){
	var user = auth(req);
	 if (!user || !admins[user.name] || admins[user.name].password !== user.pass) {
	   res.set('WWW-Authenticate', 'Basic realm="Admin panel"');
	   return res.status(401).send();
	 }
	 res.sendFile(__dirname + '/views/quote.html');
});

app.post('/changeQuotes', function(req, res, next){
	var action = req.body.action;
	if (action == 'get') {
		res.writeHead(200, {
  			'Content-Type': 'text/plain'
  		});
		var quote = JSON.parse(fs.readFileSync('quote.json', 'utf8'));
		res.end(JSON.stringify(quote));
	}else if(action == 'edit'){
		var quote = req.body.quote;
		res.writeHead(200);
		if(isJson(quote)){
			quote = JSON.parse(quote);
			quote = JSON.stringify(quote, null, 4);

			fs.writeFile('quote.json', quote, { encoding: 'utf8' },  function (err) {
			  if (err) throw err;
			  res.end('ok');
			});
		}
		else {
			res.end('error');
		}
	}
	else next();
});

app.get('/changeImages', function(req, res, next){
	var user = auth(req);
	 if (!user || !admins[user.name] || admins[user.name].password !== user.pass) {
	   res.set('WWW-Authenticate', 'Basic realm="Admin panel"');
	   return res.status(401).send();
	 }
	 res.sendFile(__dirname + '/views/image.html');
});

app.post('/changeImages', function(req, res, next){
	var action = req.body.action;
	if (action == 'get') {
		res.writeHead(200);
		var images = fs.readdirSync('public/img/');
		images = removeThumbs(images);
		res.end(images.toString());
	}else if(action == 'remove'){
		var name = req.body.file;
		res.writeHead(200);
		if(name){
			fs.unlinkSync('public/img/'+name);
			res.end('ok');
		}
		else {
			res.end('error');
		}
	}
	else next();
});

app.post('/upload', function(req, res, next) {
   	// create a form to begin parsing
    var form = new multiparty.Form();
    var uploadFile = {uploadPath: '', type: '', size: 0};
    var maxSize = 2 * 1024 * 1024; //2MB
    var supportMimeTypes = ['image/jpg', 'image/jpeg', 'image/png', 'image/gif', 'image/pjpeg'];
    var errors = [];
    var fname;

    form.on('error', function(err){
        if(fs.existsSync(uploadFile.path)) {
            fs.unlinkSync(uploadFile.path);
            console.log('error');
        }
    });

    form.on('close', function() {
        if(errors.length == 0) {
            res.send({status: 'ok', text: 'Success', file: fname});
        }
        else {
            if(fs.existsSync(uploadFile.path)) {
                fs.unlinkSync(uploadFile.path);
            }
            res.send({status: 'bad', errors: errors});
        }
    });

    // listen on part event for image file
    form.on('part', function(part) {
        uploadFile.size = part.byteCount;
        uploadFile.type = part.headers['content-type'];
        var name = part.filename.slice(0, -4);
        fname = name + '_' +  new Date().getTime() + path.extname(part.filename);
        uploadFile.path = './public/img/user-images/' + fname;

        if(uploadFile.size > maxSize) {
            errors.push('File size is ' + uploadFile.size / 1024 / 1024 + '. Limit is' + (maxSize / 1024 / 1024) + 'MB.');
        }

        if(supportMimeTypes.indexOf(uploadFile.type) == -1) {
            errors.push('Unsupported mimetype ' + uploadFile.type);
        }

        if(errors.length == 0) {
            var out = fs.createWriteStream(uploadFile.path, { flags: 'w+' });
            part.pipe(out);
        }
        else {
            part.resume();
        }
    });

    // parse the form
    form.parse(req);
});

app.post('/uploadAdmin', function(req, res, next) {
   	// create a form to begin parsing
    var form = new multiparty.Form();
    var uploadFile = {uploadPath: '', type: '', size: 0};
    var maxSize = 2 * 1024 * 1024; //2MB
    var supportMimeTypes = ['image/jpg', 'image/jpeg', 'image/png', 'image/gif', 'image/pjpeg'];
    var errors = [];
    var fname;

    form.on('error', function(err){
        if(fs.existsSync(uploadFile.path)) {
            fs.unlinkSync(uploadFile.path);
            console.log('error');
        }
    });

    form.on('close', function() {
        if(errors.length == 0) {
            res.send({status: 'ok', text: 'Success', file: fname });
        }
        else {
            if(fs.existsSync(uploadFile.path)) {
                fs.unlinkSync(uploadFile.path);
            }
            res.send({status: 'bad', errors: errors});
        }
    });

    // listen on part event for image file
    form.on('part', function(part) {
        uploadFile.size = part.byteCount;
        uploadFile.type = part.headers['content-type'];
        
        fname = new Date().getTime() + path.extname(part.filename);

        uploadFile.path = './public/img/' + fname;

        if(uploadFile.size > maxSize) {
            errors.push('File size is ' + uploadFile.size / 1024 / 1024 + '. Limit is' + (maxSize / 1024 / 1024) + 'MB.');
        }

        if(supportMimeTypes.indexOf(uploadFile.type) == -1) {
            errors.push('Unsupported mimetype ' + uploadFile.type);
        }

        if(errors.length == 0) {
            var out = fs.createWriteStream(uploadFile.path);
            part.pipe(out);
        }
        else {
            part.resume();
        }
    });

    // parse the form
    form.parse(req);
});

app.use('/', function(req, res, next) {
	//res.render('index', { title: 'Express' });
	res.sendFile(__dirname + '/views/index.html');
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.sendFile(__dirname + '/views/error.html');
});


module.exports = app;
