/*var qoutes = [
	'\u201CIt does not matter how slowly you go as long as you do not stop.\u201D\r\n \r\n\u2013 Confucius',
	'\u201CAn unexamined life is not worth living.\u201D\r\n \r\n\u2013 Socrates',
	'\u201CLife isn\u2019t about getting and having, it\u2019s about giving and being.\u201D\r\n \r\n\u2013 Kevin Kruse',
	'\u201CThink like a wise man but communicate in the language of the people.\u201D\r\n \r\n- William Butler Yeats',
	'\u201CThe best thing I did was choose the right heroes.\u201D\r\n \r\n- Warren Buffett'
];*/
var qoutes = null;
var Images = null;
var TintNumber = null;
var bgTypeNumber = null;
var bgTypeLastFilter = null;
var background = null;
var lastQoute = null;
var myText = null;
var myText2 = null;
var Logo = null;
var myTextFontFamily = 'Merriweather';
var myText2FontFamily = 'Merriweather';
var myTextFontSize = 20;
var myText2FontSize = 20;
var myTextFontColor = '#fff';
var myText2FontColor = '#fff';
var myTextFontStyle = 'normal';
var myText2FontStyle  = 'normal';
var myTextFontWeight = 400;
var myText2FontWeight  = 400;


function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function getRandomPic() {
	var length = $('.b-images__item').find('img').length;
	return getRandomInt(0, length-1);
}

function isNumber(n) {
	return !isNaN(parseFloat(n)) && isFinite(n);
}

function isInt(n) {
   return n % 1 === 0;
}

function removeNotNumbers(arr){
  for(var i=0;i<arr.length;i++){
    var name = arr[i].split('.')[0];
    if(!isNumber(name)) {
      arr.splice(i, 1);
      removeNotNumbers(arr);
    }
  }
  return arr;
}

function getQoutes() {
	var u = '\u201D\r\n \r\n';
	
	$.post('/changeQuotes', { action: 'get' }, function(data){
		qoutes = JSON.parse(data);
		qoutes.reverse();

		for (var i = 0; i < qoutes.length; i++) {
			qoutes[i] = qoutes[i].quote + u + qoutes[i].author;
		}
		getImages();
	});
}

function getImages(){
 $.post('/changeImages', { action: 'get' }, function(r){
	Images = r.split(',');
	Images = removeNotNumbers(Images);
	Images = Images.sort(function sortFunction(a, b){
	   if(+a.split('.')[0] < +b.split('.')[0]) return -1;
	   if(+a.split('.')[0] > +b.split('.')[0]) return 1;
	   return 0;
	});
	putImages(Images);
 });
}

function putImages(Images){
  var part1,part2;
  if(isInt(Images.length / 2)){
	part1 = Images.length / 2;
	part2 = Images.length;
  }
  else {
  	var tmp = Images.length - 1;
  	part1 = (tmp / 2) + 1;
	part2 = Images.length;
  }
  for (var i = 0; i < part1; i++) {
  	$('.b-images__div').first().append('<li class="b-images__item"><img class="js-change-background" src="img/'+Images[i]+'", alt="background photo" /></li">');
  }
  for (i = part1 - 1; i < part2; i++) {
  	$('.b-images__div').last().append('<li class="b-images__item"><img class="js-change-background" src="img/'+Images[i]+'", alt="background photo" /></li">');
  }

  if (getCookie('files')) showLastTimeUpload(getCookie('files'));
  drawBackground();
}

function drawBackground(){
	var rand = getRandomPic();
	imgElement = $('.b-images__item').find('img').get(rand);

	$('.b-images__item').find('img').eq(rand).parent().addClass('ss-check');

	fabric.Image.fromURL(imgElement.src, function (bg) {
		window.background = bg;
		bg.hasRotatingPoint = false;
		bg.lockRotation = true;
		bg.hasControls = false;
		bg.width = 512;
	 	bg.height = 256;
	 	bg.filters.push(new fabric.Image.filters.Tint({
	 		color: '#000000',
	 		opacity: 0.2
	 	}));
	 	TintNumber = bg.filters.length - 1;
  		bg.applyFilters(mainCanvas.renderAll.bind(mainCanvas));
	 	mainCanvas.add(bg);
	 	randomQoute();
	});
    $('.js-loading-div').fadeOut();
}

function randomQoute() {
	var rand = getRandomInt(0, qoutes.length - 1);
	var randQoute = qoutes[rand];
	if (lastQoute === randQoute) randomQoute();
	else {
		lastQoute = randQoute;
		$('.js-qoute').val(randQoute);
		if(!myText){
			window.myText = new fabric.Text(randQoute, {
				fontFamily: myTextFontFamily,
				fontSize: myTextFontSize,
				fontStyle: myTextFontStyle,
				fontWeight: myTextFontWeight,
				fill: myTextFontColor,
				left: 10,
				top: 10
			});
			mainCanvas.add(myText);
		} else {
			mainCanvas.remove(myText);
			window.myText = new fabric.Text(randQoute, {
				fontFamily: myTextFontFamily,
				fontSize: myTextFontSize,
				fontStyle: myTextFontStyle,
				fontWeight: myTextFontWeight,
				fill: myTextFontColor,
				left: 10,
				top: 10
			});
			mainCanvas.add(myText);
		}
		$('.js-font-family-current').text(myTextFontFamily).css('font-family', myTextFontFamily);
		var size = (myTextFontSize == 20) ? 'Small' : (myTextFontSize == 22) ? 'Medium' : (myTextFontSize == 24) ? 'Large' : (myTextFontSize == 26)  ? 'X-Large' : 20;

		$('.js-font-size-current').text(size);
		$('.js-font-color-current').css('background', myTextFontColor);
	}
}

function isImage(file){
	return file.type.indexOf("image") > -1;
}

function checkSize(file){
	return file.size <= 2097152;
}

function getCookie(name) {
	var cookie = " " + document.cookie;
	var search = " " + name + "=";
	var setStr = null;
	var offset = 0;
	var end = 0;
	if (cookie.length > 0) {
		offset = cookie.indexOf(search);
		if (offset != -1) {
		  offset += search.length;
		  end = cookie.indexOf(";", offset);
		  if (end == -1) {
		    end = cookie.length;
		  }
		  setStr = unescape(cookie.substring(offset, end));
		}
	}
	return(setStr);
}

function UploadFile(file, isBG) {
	var data = new FormData();
    
    data.append('uploadFile', file);

	$.ajax({
	    url: '/upload',
	    data: data,
	    cache: false,
	    contentType: false,
	    processData: false,
	    type: 'POST',
	    success: function (response) {
	        if(isBG){
	        	var filesCook = getCookie('files');
	        	if(filesCook) filesCook = filesCook + ',' + response.file;
	        	else filesCook = response.file;
	        	var date = new Date();
	        	date.setFullYear(date.setFullYear() + 10);
	        	date = new Date(date);

	        	document.cookie = "files="+ filesCook +"; path=/; expires="+date.toUTCString();
	        }
	    }
	});
}

function showLastTimeUpload(cooks) {
   cooks = cooks.split(',');
   console.log(cooks);
   $.each(cooks, function(index, value){
		$('.b-images__item_upload').after('<li class="b-images__item"><img class="js-change-background" src="img/user-images/'+ value +'" /></li>');
   });   
}

function showNotification(message){
	$('.js-notification').text(message);
	$('.js-notification').slideDown();
	setTimeout(hideNotification, 8000);
}

function hideNotification(){
	$('.js-notification').slideUp();
	$('.js-notification').text('');
}

/*
$(window).ready(function(){
	$('.js-loading-div').fadeOut();
});*/

$(function(){
	window.mainCanvas = new fabric.Canvas('mainCanvas', { backgroundColor : "#909493",width: '512',height: '256' });
	getQoutes();
});
